import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './model/user';
import {
  concatMap,
  debounce,
  debounceTime,
  delay,
  distinctUntilChanged,
  exhaustMap, filter,
  map,
  mergeMap,
  switchMap
} from 'rxjs/operators';
import { combineLatest, EMPTY, forkJoin, fromEvent, iif, Observable } from 'rxjs';
import { UserService } from './service/user.service';
import { FormControl } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'alt-root',
  template: `
    <alt-navbar></alt-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent  {
 //  input: FormControl = new FormControl('');
  // @ViewChild('myInput') input!: ElementRef<HTMLInputElement>

  constructor(
    public router: Router,
    public http: HttpClient, public userService: UserService
  ) {
    this.router.events
      .pipe(
        filter((e: any) => e instanceof NavigationEnd),
        map((e: NavigationEnd) => e.url),
      )
      .subscribe((e) => {
      })
    // fromEvent(this.input.nativeElement, 'input')
    /*this.input.valueChanges
      .pipe(
        // map((e: Event) => (e.currentTarget as HTMLInputElement).value),
        debounceTime(1000),
        distinctUntilChanged(),
        // filter(text => text),
        switchMap(
          (text) => iif(
            () => !!text,
            this.http.get<User>('https://jsonplaceholder.typicode.com/users?q=' + text),
            EMPTY
          )
        )
      )
      .subscribe(val => console.log(val))*/
  }

  ngAfterViewInit(): void {

   /*
    http.get<any>('https://jsonplaceholder.typicode.com/posts/1')
      .pipe(
        switchMap(post => http.get<User>('https://jsonplaceholder.typicode.com/users/' + post.userId))
      )*/

  }
}
