import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BgDirective } from './bg.directive';



@NgModule({
  declarations: [
    BgDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    BgDirective,
    FormsModule,
  ]
})
export class SharedModule { }
