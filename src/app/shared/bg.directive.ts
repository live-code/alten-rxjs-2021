import { Directive, ElementRef, HostBinding, HostListener, Input, Renderer2 } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';

@Directive({
  selector: '[url]'
})
export class BgDirective {
  @Input() url!: string;

  @Input() set color(val: string) {
    this.renderer2.setStyle(this.el.nativeElement, 'color', val)
  }

  /*
  @HostBinding('style.color') get color() {
    return myService.value ? .... : ...
  }
  */

  @HostListener('click')
  doSomething() {
    window.open(this.url)
  }

  constructor(
    private el: ElementRef,
    private renderer2: Renderer2,
    private authService: AuthService
  ) {
    this.authService.isLogged$
      .subscribe(val => {
        renderer2.setStyle(
          el.nativeElement,
          'display',
          val ? 'block' : 'none'
        )
      })
  }
}
