import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { concatMap, delay, map, mergeMap, switchMap, take, tap, toArray, withLatestFrom } from 'rxjs/operators';
import { BehaviorSubject, Subject } from 'rxjs';

@Component({
  selector: 'alt-users',
  template: `

    <button (click)="todoFilter$.next(true)">completed</button>
    <button (click)="todoFilter$.next(false)">todo</button>

    
    <div *ngIf="!list; else result">loading...</div>

    <ng-template #result>
      RESULTS: {{list.length}} users
      <li *ngFor="let item of list">
        {{item.user.name}}
        <ul>
          <li *ngFor="let todo of item.todos">
            <input type="checkbox" [ngModel]="todo.completed">
            {{todo.title}}
          </li>
        </ul>
      </li>
    </ng-template>
    
  `,
})
export class UsersComponent implements OnInit {
  list: { user: User, todos: any[]}[] = [];
  todoFilter$ = new BehaviorSubject(false)
  pippoFilter$ = new BehaviorSubject(false)

  constructor(private http: HttpClient) {
    // combineLast with multiple
    this.todoFilter$
      .pipe(
        switchMap(completed => {
          return http.get<User[]>('https://jsonplaceholder.typicode.com/users')
            .pipe(
              switchMap(users => users),
              concatMap(
                user => http.get<any[]>('https://jsonplaceholder.typicode.com/todos?userId=' + user.id)
                  .pipe(
                    map((todos) =>  ({
                      user,
                      todos: todos.filter(t => t.completed === completed)
                    }))
                  )
              ),
              toArray()
            )
        })
      )
      .subscribe(
        val => this.list = val
      )
   /* http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        switchMap(users => users),
        concatMap(
          user => http.get<any[]>('https://jsonplaceholder.typicode.com/todos?userId=' + user.id)
            .pipe(
              // delay(1500),
              withLatestFrom(this.todoFilter$),
              map(([todos, completed]) =>  ({
                user,
                todos: todos.filter(t => t.completed === completed)
              }))
            )
        ),
       // toArray()
      )
      .subscribe(val => {
        console.log(val)
        this.list.push(val)
        // this.list = val;
      })*/
  }

  ngOnInit(): void {
  }

}
