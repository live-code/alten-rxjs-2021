import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'alt-catalog',
  template: `
    <p>
      catalog works!
    </p>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor(http: HttpClient) {
    http.get('http://localhost:3000/catalog')
      .subscribe(
        res => console.log(res),
        err => alert('errore!')
      )
  }

  ngOnInit(): void {
  }

}
