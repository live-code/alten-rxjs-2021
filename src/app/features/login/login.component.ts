import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'alt-login',
  template: `
    <input type="text" [ngModel]>
    <button (click)="loginhandler()">Login</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
  }

  loginhandler() {
    this.authService.login('pippo', '123')
  }
}
