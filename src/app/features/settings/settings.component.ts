import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'alt-settings',
  template: `
    <p>
      settings works!
    </p>
    {{themeService.theme$ | async}}
    <button (click)="themeService.value = 'light'">Light</button>
    <button (click)="themeService.value = 'dark'">dark</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {
    this.themeService.theme$.subscribe(console.log)
  }

  ngOnInit(): void {
  }

}
