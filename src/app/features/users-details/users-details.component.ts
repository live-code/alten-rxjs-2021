import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';

const baseUrl = 'https://jsonplaceholder.typicode.com/users';

@Component({
  selector: 'alt-users-details',
  template: `
    <button (click)="gotoNext()">Next</button>
    {{data | json}}
    
  `,
  styles: [
  ]
})
export class UsersDetailsComponent {
  data!: User;

  constructor(private http: HttpClient, private activateRoute: ActivatedRoute, private router: Router) {

    this.activateRoute.params
      .pipe(
        switchMap(({ id }) => this.http.get<User>(`${baseUrl}/${id}`) )
      )
      .subscribe(res => {
         this.data = res;
      })
  }


  gotoNext(): void {
    this.router.navigateByUrl('/users/' + (+this.activateRoute.snapshot.params.id + 1));
  }
}
