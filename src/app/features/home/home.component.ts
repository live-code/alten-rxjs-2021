import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'alt-home',
  template: `
    <button url="http://www.google.com" [color]="color">
      home works!
    </button>
    
    <button (click)="value = 'SM'">SM</button>
    <button (click)="value = 'XL'">XL</button>
    <button (click)="color = 'blue'">blue</button>
    <button (click)="color = 'yellow'">yellow</button>
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  value: 'SM' | 'XL' = 'XL'
  color: 'blue' | 'yellow' = 'blue'
  constructor() { }

  ngOnInit(): void {
  }

}
