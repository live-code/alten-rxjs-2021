import { BehaviorSubject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Auth } from './auth';
import { map, withLatestFrom } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root'})
export class AuthService {
  data$ = new BehaviorSubject<Auth | null>(null)

  constructor(private http: HttpClient, private router: Router) { }

  login(email: string, pass: string) {
    this.http.get<Auth>('http://localhost:3000/login')
      .subscribe(res => {
        this.data$.next(res);
      })

  }
  
  logout() {
    this.data$.next(null);
    this.router.navigateByUrl('login')
  }
  
  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        map(auth => !!auth),
      )
  }

  get role$(): Observable<string | undefined> {
    return this.data$
      .pipe(
        map(auth => auth?.role)
      )
  }

  get token$(): Observable<string | undefined> {
    return this.data$
      .pipe(
        map(auth => auth?.token)
      )
  }

  get displayName$(): Observable<string> {
    return this.data$
      .pipe(
          map(auth => auth?.displayName || '')
      )
  }
}
