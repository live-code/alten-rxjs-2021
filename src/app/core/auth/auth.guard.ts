import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root'})
export class AuthGuard implements CanActivate {
  constructor(
    private http: HttpClient,
    private router: Router, private authService: AuthService) {
  }

  canActivate(): Observable<boolean> {
    // ASYNC
    /*return this.http.get<{ response: string}>('http://localhost:3000/validateToken')
      .pipe(
        map(res => res.response === 'ok'),
        tap(value => {
          if(!value) {
            this.router.navigateByUrl('login')
          }
        })
      )*/

    // SYNC
    return this.authService.isLogged$
      .pipe(
        tap(value => {
          if(!value) {
            this.router.navigateByUrl('login')
          }
        })
      )
  }
}
