import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[altIfLogged]'
})
export class IfLoggedDirective {

  constructor(
    private view: ViewContainerRef,
    private template: TemplateRef<any>,
    private authService: AuthService
  ) {
    this.authService.isLogged$
      .subscribe(val => {
        if (val) {
          view.createEmbeddedView(template)
        } else {
          view.clear();
        }
      })

  }

}
