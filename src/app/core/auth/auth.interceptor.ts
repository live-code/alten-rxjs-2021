import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { iif, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, filter, first, switchMap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        switchMap(([isLogged, token]) => {
          return isLogged && token ?
            next.handle(request.clone({ setHeaders: { Authorization: token}})) :
            next.handle(request)
        }),
        catchError(err => {
          if (err instanceof  HttpErrorResponse) {
            switch (err.status) {
              case 401:
              case 404:
              default:
                // redirect login
                this.authService.logout();
                break;
            }
          }

          return throwError(err);
        })
      )
  }


  intercept2(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let cloneReq = request;
    if (this.authService.data$.getValue()) {
      const tk =  this.authService.data$.getValue()?.token || ''
      cloneReq = request.clone({ setHeaders: { Authorization: tk}})
    }
    return next.handle(cloneReq);
  }
}
