import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'alt-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div
      *ngIf="(themeService.theme$ | async) as theme"
      class="nav"
      [ngClass]="{
        'dark': theme === 'dark',
        'light': theme === 'light'
      }"
    >
      <button routerLink="login">login</button>
      <button routerLink="users" *altIfLogged>users</button>
      <button routerLink="users/1" *altIfLogged>user 1</button>
      
      <button routerLink="catalog" *altIfLogged>catalog</button>
      <button routerLink="home">home</button>
      <button routerLink="settings" *altIfLogged>settings</button>
      
    
      <button
        *altIfLogged
        (click)="authService.logout()">lgout</button>

      <alt-displayname
        color="blue"
        [text]="(authService.displayName$ | async)"></alt-displayname>
    <!--  {{(authService.displayName$ | async)}}-->
    </div>
    
    
    {{render()}}

  `,
  styles: [`
    .nav { padding: 20px}
    .dark {
      background-color: #222; color: white;
    }
    .light {
      background-color: #ccc;
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor(public themeService: ThemeService, public authService: AuthService) { }

  ngOnInit(): void {
  }

  render() {
    console.log('render')
  }
}
