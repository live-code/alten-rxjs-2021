import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'alt-displayname',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <span *ngIf="displayName" 
          style="padding: 10px; border-radius: 10px"
          [style.background-color]="color"
    >
      {{displayName}}
    </span>
  `,
  styles: [
  ]
})
export class DisplaynameComponent implements OnChanges {
  // @Input() text: string | null = null;
  displayName!: string | null;
  @Input() set text(val: string | null) {
    console.log('action!')
    this.displayName = val;
  }

  @Input() color!: string;

  ngOnChanges() {
    console.log('ngOnChanges', this.color, this.displayName)
  }
}
