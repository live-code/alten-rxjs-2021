import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export type Theme = 'dark' | 'light';

const obj: { [key: string]: Theme} = {
  DARK: 'dark'
}

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private _theme$ = new BehaviorSubject<Theme>(obj.DARK)
  theme$ = this._theme$.asObservable();

  set value(val: Theme) {
    this._theme$.next(val)
  }

}
